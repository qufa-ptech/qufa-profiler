CREATE TABLE IF NOT EXISTS result_vf (
    id int primary key auto_increment,
    column_name varchar(255) null,
    column_desc varchar(255) null,
    column_group_val varchar(255) null,
    column_group_cnt int null,
    result_id int null,
    foreign key (result_id) references result (id)
);
