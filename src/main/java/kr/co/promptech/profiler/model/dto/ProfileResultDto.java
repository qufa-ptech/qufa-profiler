package kr.co.promptech.profiler.model.dto;

import kr.co.promptech.profiler.model.profile.History;
import kr.co.promptech.profiler.model.profile.Result;
import lombok.Data;

import java.util.List;

@Data
public class ProfileResultDto {
    private String columnName;
    private String columnDesc;
    private String columnType;
    private Integer rowCnt;
    private Integer nullCnt;
    private Integer uniqueCnt;
    private Integer distinctCnt;
    private Integer duplicateCnt;
    private Integer blankCnt;
    private String frquentMinVal;
    private String frquentMaxVal;

    // Boolean 타입 한정
    private Integer trueCnt;
    private Integer falseCnt;

    // String 타입 한정
    private Integer strMinLenVal;
    private Integer strMaxLenVal;
    private Double strAvgLenVal;

    // Number 타입 한정
    private Double numMinVal;
    private Double numMaxVal;
    private Double numMedianVal;
    private Double numMeanVal;

    List<ValueFrequentDto> vfs;

    public static ProfileResultDto objectToDTO(Result result) {
        ProfileResultDto resultDto = new ProfileResultDto();

        resultDto.setColumnName(result.getColumnName());
        resultDto.setColumnDesc(result.getColumnDesc());
        resultDto.setColumnType(result.getColumnType());
        resultDto.setRowCnt(result.getRowCnt());
        resultDto.setNullCnt(result.getNullCnt());
        resultDto.setUniqueCnt(result.getUniqueCnt());
        resultDto.setDistinctCnt(result.getDistinctCnt());
        resultDto.setDuplicateCnt(result.getDuplicateCnt());
        resultDto.setBlankCnt(result.getBlankCnt());
        resultDto.setFrquentMinVal(result.getFrquentMinVal());
        resultDto.setFrquentMaxVal(result.getFrquentMaxVal());
        resultDto.setTrueCnt(result.getTrueCnt());
        resultDto.setFalseCnt(result.getFalseCnt());
        resultDto.setStrMinLenVal(result.getStrMinLenVal());
        resultDto.setStrMaxLenVal(result.getStrMaxLenVal());
        resultDto.setStrAvgLenVal(result.getStrAvgLenVal());
        resultDto.setNumMinVal(result.getNumMinVal());
        resultDto.setNumMaxVal(result.getNumMaxVal());
        resultDto.setNumMedianVal(result.getNumMedianVal());
        resultDto.setNumMeanVal(result.getNumMeanVal());

        return resultDto;
    }
}
