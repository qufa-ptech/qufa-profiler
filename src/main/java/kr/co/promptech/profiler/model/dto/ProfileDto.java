package kr.co.promptech.profiler.model.dto;

import kr.co.promptech.profiler.model.profile.History;
import kr.co.promptech.profiler.model.profile.Result;
import kr.co.promptech.profiler.model.profile.ResultVF;
import kr.co.promptech.profiler.model.status.HistoryStatus;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProfileDto {
    private String status;

    private List<ProfileResultDto> results;

    public static ProfileDto objectToDTO(History history) {
        ProfileDto profileDto = new ProfileDto();
        if (history != null) {
            profileDto.setStatus(history.getStatus().name());

            if (history.getStatus().equals(HistoryStatus.SUCCESS)) {
                List<ProfileResultDto> resultDtos = new ArrayList<>();

                for (Result r : history.getResults()) {
                    ProfileResultDto resultDto = ProfileResultDto.objectToDTO(r);

                    List<ValueFrequentDto> valueFrequentDtos = new ArrayList<>();
                    if (r.getDistinctCnt() < Result.PERMIT_MIN_VF_COUNT) {
                        for (ResultVF vf : r.getResultVFList()) {
                            valueFrequentDtos.add(ValueFrequentDto.objectToDTO(vf));
                        }
                        resultDto.setVfs(valueFrequentDtos);
                    }


                    resultDtos.add(resultDto);
                }

                profileDto.setResults(resultDtos);
            }
        }

        return profileDto;
    }
}
