package kr.co.promptech.profiler.model.dto;

import kr.co.promptech.profiler.model.profile.Result;
import kr.co.promptech.profiler.model.profile.ResultVF;
import lombok.Data;

@Data
public class ValueFrequentDto {
    private String columnGroupVal;
    private Integer columnGroupCount;

    public static ValueFrequentDto objectToDTO(ResultVF vf) {
        ValueFrequentDto vfDTO = new ValueFrequentDto();
        vfDTO.setColumnGroupCount(vf.getColumnGroupCount());
        vfDTO.setColumnGroupVal(vf.getColumnGroupVal());

        return vfDTO;
    }
}
