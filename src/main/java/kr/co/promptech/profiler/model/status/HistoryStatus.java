package kr.co.promptech.profiler.model.status;

public enum HistoryStatus {
    PENDING,
    SUCCESS,
    FAILED
}
