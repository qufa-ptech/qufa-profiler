package kr.co.promptech.profiler.utils.logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class GlobalLoggerFactory {
    private Logger logger;

    private GlobalLoggerFactory() {
    }

    private static class Singleton {
        private static final GlobalLoggerFactory instance = new GlobalLoggerFactory();
    }

    public static GlobalLoggerFactory getInstance(Class<?> c) {
        Singleton.instance.setClass(c);

        return Singleton.instance;
    }

    public void setClass(Class<?> c) {
        this.logger = LogManager.getLogger(c);
    }


    public String logException(Exception e, LogType type) {
        StringBuffer sb = new StringBuffer();

        if (e instanceof SQLException) {
            sb.append(String.format("\nSQLState: %s\n", ((SQLException) e).getSQLState()));
            sb.append(String.format("Error Code: %s\n", ((SQLException) e).getErrorCode()));
        }

        sb.append(String.format("%s : %s", e.getClass(), e.getMessage()));
        for (StackTraceElement el : e.getStackTrace()) {
            sb.append("\n\t at ");
            sb.append(el);
        }

        String msg = sb.toString();

        this.simpleLog(msg, type);

        return msg;
    }

    public void simpleLog(String msg, LogType type) {
        switch (type) {
            case INFO:
                logger.info(msg);
                break;
            case ERROR:
                logger.error(msg);
                break;
            case WARN:
                logger.warn(msg);
                break;
            case DEBUG:
                logger.debug(msg);
                break;
        }
    }
}
