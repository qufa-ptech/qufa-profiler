package kr.co.promptech.profiler.reopository.meta;

import kr.co.promptech.profiler.model.meta.MetaColumn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * meta_column에 대한 JpaRepository 클래스
 */
@Repository
public interface MetaColumnRepository extends JpaRepository<MetaColumn, Integer> {
    List<MetaColumn> findAllByMetaId(int id);

    @Query(value = "SELECT mc.originalColumnName FROM meta_column AS mc JOIN meta AS mt ON mc.metaId = mt.id JOIN service AS sv ON sv.metaId = mt.id WHERE sv.id = :id", nativeQuery = true)
    List<String> findAllColumnsByServiceId(@Param("id") int id);

    @Query(value = "SELECT COLUMN_NAME FROM information_schema.columns WHERE TABLE_NAME = :table_name AND TABLE_SCHEMA = :table_schema", nativeQuery = true)
    List<String> findAllColumns(@Param("table_name") String tableName, @Param("table_schema") String tableSchema);
}
