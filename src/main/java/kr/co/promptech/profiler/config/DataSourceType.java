package kr.co.promptech.profiler.config;

public enum DataSourceType {
    META,
    DATASET,
    PROFILER
}
